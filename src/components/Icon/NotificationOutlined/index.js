import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function NotificationOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M17.852.352a2.152 2.152 0 00-2.111 1.75C11.822 3.767 5.688 3.789 5.625 3.789H2.93A2.933 2.933 0 000 6.72V9.53a2.933 2.933 0 002.93 2.93h2.22l1.036 4.856a2.942 2.942 0 002.868 2.331 1.577 1.577 0 001.544-1.899l-.423-2.033a3.727 3.727 0 002.404-2.463c1.098.201 2.168.476 3.162.896a2.152 2.152 0 002.11 1.75A2.15 2.15 0 0020 13.75V2.5A2.15 2.15 0 0017.852.352zm-5.62 11.654c-.647-.121-3.966-.543-6.021-.683V4.951c1.597-.043 6.087-.28 9.492-1.571v9.493c-1.112-.412-2.296-.675-3.471-.867zM1.172 9.531V6.72c0-.97.788-1.758 1.758-1.758h2.11v6.328H2.93a1.76 1.76 0 01-1.758-1.758zm7.882 8.946a1.766 1.766 0 01-1.721-1.402l-.974-4.566c.646.047 1.323.109 1.989.182l1.103 5.297a.405.405 0 01-.397.489zm.88-3.919l-.36-1.724a71.3 71.3 0 011.832.23 2.55 2.55 0 01-1.473 1.494zm8.894-.808a.978.978 0 01-1.953 0V2.5a.978.978 0 011.953 0v11.25z"
        fill={color}
      />
    </Svg>
  );
}
