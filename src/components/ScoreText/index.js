import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

export default function ScoreText(props) {
  const { label1, label2 } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label1}>{label1}</Text>
      <Text style={styles.label2}>{label2}</Text>
    </View>
  );
}
