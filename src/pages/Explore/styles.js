import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: { flex: 1 },
  cardSimple: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 16,
    paddingLeft: 24,
  },
  textHashTagSimple: { flex: 1, paddingHorizontal: 8, color: '#2B3657', fontWeight: '600' },
  textViewSimple: { color: '#7180AC', fontWeight: '600' },
  cardHashTag: {
    backgroundColor: '#E5E7FA',
    padding: 6,
    paddingHorizontal: 20,
    flexGrow: 4,
    margin: 4,
    alignItems: 'center',
    borderRadius: 30,
  },
  textHashTag: { color: '#6979F8', fontSize: 11 },
  cardSuggestion: { marginRight: 8 },
  imageSuggestion: { borderRadius: 8, width: 100, height: 128 },
  textSuggestion: { position: 'absolute', top: 0, padding: 10, color: '#fff', fontWeight: '600' },
  titleContainer: {
    padding: 16,
  },
  simpleContainer: {
    backgroundColor: '#fff',
  },
  hasTagContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 16,
    paddingVertical: 16,
    backgroundColor: '#fff',
  },
  suggestionContainer: { paddingHorizontal: 16, paddingBottom: 16 },
  svSuggestion: { paddingBottom: 10 },
  suggestionItem: { flexDirection: 'row' },
});

export default styles;
