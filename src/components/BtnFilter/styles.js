import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  btnContainer: {
    backgroundColor: '#E5E7FA',
    padding: 5,
    borderRadius: 30,
    minWidth: 109
  },
  label: {
    textAlign: 'center',
    fontSize: 11,
    color: '#6979F8',
  },
});

export default styles;
