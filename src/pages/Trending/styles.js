import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  trendingContainerLayout: {
    flex: 1,
    marginTop: 88,
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  titleLayout: {
    marginBottom: 10
  },

  filterLayout: {
    flexDirection: 'row',
    marginHorizontal: -4,
    marginBottom: 20
  },
  itemFilterWrap: {
    marginHorizontal: 4
  },

  cateLayout: {

  }
});

export default styles;
