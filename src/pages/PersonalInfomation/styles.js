import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  personalContainerLayout: {
    flex: 1,
    paddingTop: 88,
    paddingVertical: 30,
  },
  avatarLayout: {
    marginBottom: 26,
  },
  formContainerLayout: {
    paddingLeft: 24,
  },
  formBlockLayout: {
    marginBottom: 24
  },
  titleWrap: {
    marginBottom: 7,
  },
  titleFormGroup: {
    fontSize: 16,
    fontWeight: '600',
    color: '#2B3657',
  },
});

export default styles;
