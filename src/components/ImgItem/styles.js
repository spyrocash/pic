import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  img: {},
  type: {
    position: 'absolute',
    top: 8,
    right: 6.5,
    zIndex: 2,
  },
  imgType: {
    width: 14,
    height: 9,
  },
  txtVideo: {
    fontSize: 10,
    fontWeight: '600',
    color: '#FFFFFF',
  },
});

export default styles;
