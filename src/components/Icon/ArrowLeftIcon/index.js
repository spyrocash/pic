import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function ArrowLeft({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 12;
  const viewBoxHeight = 23;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M11.5424 3.18332C12.1301 2.57809 12.1526 1.57247 11.5927 0.937195C11.0328 0.301921 10.1025 0.277563 9.51477 0.88279C9.51477 0.88279 0.44563 10.3672 0.420893 10.3948C-0.158688 11.0502 -0.137011 12.0893 0.46931 12.7158L9.51468 22.1707C10.1014 22.777 11.0318 22.7543 11.5927 22.1201C12.1536 21.4858 12.1326 20.4801 11.5458 19.8739L4.01979 11.8711C3.8388 11.6786 3.83877 11.3786 4.01971 11.1861L11.5424 3.18332Z"
        fill={color}
      />
    </Svg>
  );
}
