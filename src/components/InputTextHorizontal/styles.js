import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  label: {
    fontSize: 16,
    fontWeight: '500',
    color: '#2B3657',
  },
  input: {
    flex: 1,
    marginLeft: 16,
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#E2E4EF',
    fontSize: 16,
    color: '#7180AC'
  },
});

export default styles;
