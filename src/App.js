import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ThemeProvider } from 'react-native-elements';
import { StoreProvider } from 'easy-peasy';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from 'react-native-splash-screen';

import store from './store';

import theme from './configs/theme';

import Route from './Route';

const AfterStoreProvider = () => {
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    const clear = async () => {
      try {
        await AsyncStorage.clear();
      } catch {}
    };

    const init = async () => {
      // await clear();
      await Promise.all([]).catch((error) => {
        console.log('init error', error);
      });

      setIsReady(true);
    };

    init();
  }, []);

  useEffect(() => {
    if (isReady) SplashScreen.hide();
  }, [isReady]);

  if (!isReady) return null;

  return (
    <ThemeProvider theme={theme}>
      <SafeAreaProvider>
        <NavigationContainer>
          <Route />
        </NavigationContainer>
      </SafeAreaProvider>
    </ThemeProvider>
  );
};

function App() {
  return (
    <StoreProvider store={store}>
      <AfterStoreProvider />
    </StoreProvider>
  );
}

export default App;
