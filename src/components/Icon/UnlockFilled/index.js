import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function UnlockFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 14;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M12.263 8.947H3.58V5A3.425 3.425 0 017 1.579 3.425 3.425 0 0110.421 5v1.579a.79.79 0 001.579 0V5c0-2.757-2.243-5-5-5S2 2.243 2 5v3.947h-.263c-.727 0-1.316.59-1.316 1.316v8.421c0 .727.59 1.316 1.316 1.316h10.526c.727 0 1.316-.59 1.316-1.316v-8.42c0-.728-.59-1.317-1.316-1.317zm-6.842 4.474a1.579 1.579 0 112.368 1.367v2.317H6.21v-2.317a1.578 1.578 0 01-.789-1.367z"
        fill={color}
      />
    </Svg>
  );
}
