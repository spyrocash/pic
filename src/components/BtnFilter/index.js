import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

export default function BtnFilter(props) {
  const { label } = props;
  return (
    <TouchableOpacity>
      <View style={styles.btnContainer}>
        <Text style={styles.label}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}
