import React, { useState } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import styles from './styles';
import TagDialogFiled from '../../components/Icon/TagDialogFiled';

function CardSimple() {
  return (
    <View style={styles.cardSimple}>
      <TagDialogFiled />
      <Text style={styles.textHashTagSimple}>Simple List</Text>
      <Text style={styles.textViewSimple}>40.8k</Text>
    </View>
  );
}

function CardHashTag() {
  return (
    <View style={styles.cardHashTag}>
      <Text style={styles.textHashTag}>#Label</Text>
    </View>
  );
}

function CardSuggestion() {
  return (
    <View style={styles.cardSuggestion}>
      <Image source={require('../../../assets/images/nike.png')} style={styles.imageSuggestion} />
      <Text style={styles.textSuggestion}>#Shoes</Text>
    </View>
  );
}

export default function Explore() {
  const [simepleList, setSimpleList] = useState([...Array(3)]);
  const [chipsList, setChipsList] = useState([...Array(7)]);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.titleContainer}>
          <Text>Simple List</Text>
        </View>
        <View>
          {simepleList.map((i, idx) => {
            return <CardSimple key={idx} />;
          })}
        </View>
        <View style={styles.titleContainer}>
          <Text>Chips List</Text>
        </View>
        <View style={styles.hasTagContainer}>
          {chipsList.map((i, idx) => {
            return <CardHashTag key={idx} />;
          })}
        </View>
        <View style={styles.titleContainer}>
          <Text>For You</Text>
        </View>
        <View style={styles.suggestionContainer}>
          <ScrollView horizontal style={styles.svSuggestion}>
            <View style={styles.suggestionItem}>
              {chipsList.map((i, idx) => {
                return <CardSuggestion key={idx} />;
              })}
            </View>
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
}
