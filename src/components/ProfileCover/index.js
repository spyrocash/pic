import React from 'react';
import { View, Text, Image } from 'react-native';
import CrownIcon from '../Icon/CrownIcon';
import SilverStarIcon from '../Icon/SilverStarIcon';
import styles from './styles';
export default function ProfileCover({ rank = 'crown' }) {
  return (
    <View>
      <Image source={require('../../../assets/images/user_1.jpg')} style={styles.image} />
      {rank === 'crown' && (
        <View style={styles.iconRank}>
          <CrownIcon />
        </View>
      )}
      {rank === 'silver' && (
        <View style={styles.iconRank}>
          <SilverStarIcon />
        </View>
      )}
    </View>
  );
}
