import React from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from './styles';

export default function InputTextHorizontal(props) {
  const { label, labelWidth } = props;
  return (
    <View style={styles.container}>
      {label && <Text style={[styles.label, labelWidth && { width: labelWidth }]}>{label}</Text>}
      <TextInput style={styles.input} {...props} />
    </View>
  );
}
