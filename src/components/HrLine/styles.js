import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  line: {
    height: 1,
    backgroundColor: '#E2E4EF',
  },
});

export default styles;
