import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import styles from './styles';

export default function ImgItem(props) {
  const { imgPros, type, time, action } = props;

  const renderType = (key) => {
    switch (key) {
      case 'img':
        return <Image style={styles.imgType} source={require('../../assets/icons/img-type.png')} />;
      case 'video':
        return <Text style={styles.txtVideo}>{time}</Text>;
      default:
        break;
    }
  };

  const renderAction = (key) => {
    switch (key) {
      case "true":
        return <View></View>
      case "false":
      return  <View></View>
      default:
        break;
    }
  };

  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.type}>{renderType(type)}</View>
      <Image style={styles.img} {...imgPros} />
      {action && <View>{renderAction(action)}</View>}
    </TouchableOpacity>
  );
}
