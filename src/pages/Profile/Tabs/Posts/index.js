import React from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import styles from './styles';
import { ImgItem } from '../../../../components';

const windowWidth = Dimensions.get('window').width;
// const windowHeight = Dimensions.get('window').height;

const calImgWidth = windowWidth / 3 - 16;
export default function Posts() {
  return (
    <ScrollView style={styles.containerWrap}>
      <View style={styles.postContainerLayout}>
        {[0, 0, 0, 0, 0, 0, 0, 0].map((e, i) => (
          <View key={i} style={styles.itemLayout}>
            <ImgItem
              type={i % 2 === 0 ? 'video' : 'img'}
              time="0:15"
              imgPros={{
                style: { width: calImgWidth, height: calImgWidth },
                source: require('../../../../assets/images/test93x93.jpg'),
              }}
            />
          </View>
        ))}
      </View>
    </ScrollView>
  );
}
