import React from 'react';
import Svg, { G, Path, Defs, ClipPath } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function HashtagOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <G clipPath="url(#prefix__clip0)" fill={color}>
        <Path d="M14.438 7.755h-1.176l.482-1.72a.575.575 0 00-1.108-.31l-.568 2.03H9.046l.48-1.713a.575.575 0 00-1.108-.31L7.85 7.754H6.219a.575.575 0 100 1.15h1.31l-.598 2.137H5.529a.575.575 0 000 1.151h1.08l-.492 1.757a.575.575 0 00.398.71.592.592 0 00.71-.4l.578-2.067h3.022l-.49 1.75a.575.575 0 101.108.31l.577-2.06h1.728a.575.575 0 100-1.15h-1.406l.598-2.137h1.498a.575.575 0 100-1.15zm-3.29 3.287H8.124l.599-2.136h3.022l-.599 2.136z" />
        <Path d="M16.942 3.058A9.771 9.771 0 0013.764.93a9.768 9.768 0 00-3.75-.75H10c-2.623 0-5.463 1.151-7.353 3.312A9.792 9.792 0 00.182 10c0 1.724.448 3.41 1.298 4.886l-.721 2.88c-.102.408.01.835.303 1.141.39.406.845.406 1.163.332l3.073-.615A9.816 9.816 0 0010 19.818a9.791 9.791 0 006.506-2.465c2.42-2.354 3.312-4.73 3.312-7.353a9.754 9.754 0 00-2.876-6.942zm-1.199 13.434A8.642 8.642 0 0110 18.668a8.657 8.657 0 01-4.27-1.12.739.739 0 00-.508-.082l-3.233.647-.022.005a.259.259 0 01-.058.008.171.171 0 01-.016-.014.071.071 0 01-.018-.067l.762-3.043a.733.733 0 00-.083-.559A8.642 8.642 0 011.332 10c0-2.122.773-4.162 2.176-5.743A8.628 8.628 0 0110 1.332h.013a8.612 8.612 0 016.116 2.54A8.611 8.611 0 0118.668 10c0 2.316-.777 4.53-2.925 6.492z" />
      </G>
      <Defs>
        <ClipPath id="prefix__clip0">
          <Path fill="#fff" transform="translate(.182 .182)" d="M0 0h19.636v19.636H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
}
