import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  tabLabelF: {
    fontSize: 12,
    fontWeight: '500',
    color: '#2B3657',
  },
  tabLabel: {
    fontSize: 12,
    fontWeight: '500',
    color: '#B1B9D1',
  },

  indicator: {
    borderBottomWidth: 1,
    borderBottomColor: 'red',
  },
  tabContent: {
    paddingVertical: 16
  }
});

export default styles;
