import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function CheckOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 16;
  const viewBoxHeight = 12;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path d="M5.5 9.475L2.025 6 .842 7.175 5.5 11.833l10-10L14.325.658 5.5 9.475z" fill={color} />
    </Svg>
  );
}
