import React, { useState } from 'react';
import { View, SafeAreaView } from 'react-native';

import { HeaderHome, SearchBar } from '../../components';
import styles from './styles';
import Explore from '../Explore';
import { ButtonGroup } from 'react-native-elements';
import Following from '../Following';

export default function Home() {
  const [pageIndex, setPageIndex] = useState(0);
  const pages = ['Explore', 'Following'];

  return (
    <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
      <SafeAreaView />
      <View style={styles.container}>
        <HeaderHome />
        <SearchBar />
      </View>
      <View style={{ backgroundColor: '#fafafa' }}>
        <ButtonGroup
          buttons={pages}
          onPress={(idx) => setPageIndex(idx)}
          containerStyle={{ borderRadius: 8 }}
        />
      </View>
      {pageIndex == 0 ? <Explore /> : <Following />}
    </View>
  );
}
