import React from 'react';
import Svg, { G, Path, Circle, Defs, ClipPath } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function CommentFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 18;
  const viewBoxHeight = 18;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <G clipPath="url(#prefix__clip0)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M6 1A6 6 0 00.01 7.361a.203.203 0 00-.01.066V16.1a.2.2 0 00.32.16l4.501-3.376c.381.076.776.116 1.18.116h6a6 6 0 000-12H6z"
          fill={color}
        />
        <Circle cx={5} cy={7} r={1} fill="#fff" />
        <Circle cx={9} cy={7} r={1} fill="#fff" />
        <Circle cx={13} cy={7} r={1} fill="#fff" />
      </G>
      <Defs>
        <ClipPath id="prefix__clip0">
          <Path fill="#fff" transform="translate(0 .5)" d="M0 0h18v17H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
}
