import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function Serach({ width, height, color = '#8E8E93' }) {
  const viewBoxWidth = 14;
  const viewBoxHeight = 15;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        fill-rule="evenodd" clip-rule="evenodd" d="M5.50977 11.5098C2.47217 11.5098 0.00976562 9.04736 0.00976562 6.00977C0.00976562 2.97217 2.47217 0.509766 5.50977 0.509766C8.54736 0.509766 11.0098 2.97217 11.0098 6.00977C11.0098 7.30469 10.5623 8.49561 9.81335 9.43506C9.84485 9.45166 9.87524 9.47119 9.90417 9.49316C9.92688 9.51025 9.94861 9.5293 9.96936 9.55029L13.7877 13.3687C14.0416 13.6226 14.0416 14.0342 13.7877 14.2881C13.5339 14.5415 13.1223 14.5415 12.8685 14.2881L9.05017 10.4697C9.00293 10.4224 8.96436 10.3696 8.93469 10.3135C7.995 11.0625 6.80469 11.5098 5.50977 11.5098ZM10.0098 6.00977C10.0098 8.49512 7.995 10.5098 5.50977 10.5098C3.02454 10.5098 1.00977 8.49512 1.00977 6.00977C1.00977 3.52441 3.02454 1.50977 5.50977 1.50977C7.995 1.50977 10.0098 3.52441 10.0098 6.00977Z"
        fill={color}
      />
    </Svg>
  );
}
