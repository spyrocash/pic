import React, { useState } from 'react';
import { View, Text, ScrollView, FlatList } from 'react-native';
import styles from './styles';
import CardPost from '../../components/CardPost';

export default function Following() {
  const [posts, setPosts] = useState([...Array(4)]);

  return (
    <View style={styles.container}>
      <FlatList
        data={posts}
        renderItem={({ item, idx }) => {
          return <CardPost key={idx} votingMode />;
        }}
        contentContainerStyle={{ paddingVertical: 16 }}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}
