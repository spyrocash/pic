import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  leftCol: {},
  rightCol: {
    marginLeft: 16
  },
  labelWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label1: {
    fontSize: 16,
    fontWeight: '600',
    marginRight: 8,
    color: '#2B3657',
  },
  label2: {
    paddingTop: 5,
    fontSize: 16,
    fontWeight: 'normal',
    color: '#7180AC'
  },
});

export default styles;
