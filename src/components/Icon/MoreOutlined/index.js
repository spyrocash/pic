import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function MoreOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 4;
  const viewBoxHeight = 14;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M2 3.833c.87 0 1.583-.712 1.583-1.583C3.583 1.38 2.871.667 2 .667 1.13.667.417 1.379.417 2.25c0 .87.712 1.583 1.583 1.583zm0 1.584C1.13 5.417.417 6.129.417 7c0 .87.712 1.583 1.583 1.583.87 0 1.583-.712 1.583-1.583 0-.87-.712-1.583-1.583-1.583zm0 4.75c-.87 0-1.583.712-1.583 1.583 0 .87.712 1.583 1.583 1.583.87 0 1.583-.712 1.583-1.583 0-.87-.712-1.583-1.583-1.583z"
        fill={color}
      />
    </Svg>
  );
}
