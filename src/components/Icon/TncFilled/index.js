import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function TncFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 16;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M12.862 0h-1.23l-.181 1.692c-.11.914-1.1 2.019-2.468 2.019H7.017A2.531 2.531 0 014.563 1.77c-.014-.056 0 .053-.194-1.77H3.138A2.29 2.29 0 00.852 2.286v15.428A2.29 2.29 0 003.138 20h9.724a2.289 2.289 0 002.286-2.286V2.286A2.289 2.289 0 0012.862 0zm-4.55 15.586H3.939c-.776 0-.776-1.172 0-1.172h4.374c.776 0 .776 1.172 0 1.172zm0-2.5H3.939c-.776 0-.776-1.172 0-1.172h4.374c.776 0 .776 1.172 0 1.172zm0-2.5H3.939c-.776 0-.776-1.172 0-1.172h4.374c.776 0 .776 1.172 0 1.172zm0-2.5H3.939c-.776 0-.776-1.172 0-1.172h4.374c.776 0 .776 1.172 0 1.172zm4.334 6.972c-.08.773-1.245.652-1.167-.116.08-.765 1.23-.664 1.167.116zm0-2.5c-.081.776-1.245.65-1.166-.116.062-.752 1.23-.677 1.166.116zm-.169-2.143a.587.587 0 11-.829-.83.587.587 0 01.829.83zm.169-2.857c-.08.775-1.245.65-1.167-.116.081-.778 1.245-.648 1.167.116zM5.548 0l.162 1.525a1.36 1.36 0 001.307 1.014h1.966a1.36 1.36 0 001.307-1.014L10.452 0H5.548z"
        fill={color}
      />
    </Svg>
  );
}
