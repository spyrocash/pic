import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  avatarWrap: {
    marginBottom: 8,
  },
  label1: {
    fontSize: 16,
    fontWeight: '600',
    color: '#2B3657',
  },
  label2: {
    fontSize: 16,
    fontWeight: '400',
    color: '#7180AC',
  },
});

export default styles;
