import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function BarChartOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 18;
  const viewBoxHeight = 15;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path d="M3.48 5H.08v9.49h3.4V5zM10.4 0H7v14.45h3.4V0zM17.4 2H14v12.42h3.4V2z" fill={color} />
    </Svg>
  );
}
