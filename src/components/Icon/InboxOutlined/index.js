import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function InboxOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
       d="M17.7778 0H2.21111C0.977778 0 0.0111111 0.988889 0.0111111 2.22222L0 17.7778C0 19 0.977778 20 2.21111 20H17.7778C19 20 20 19 20 17.7778V2.22222C20 0.988889 19 0 17.7778 0ZM17.7778 13.3333H13.3333C13.3333 15.1778 11.8333 16.6667 10 16.6667C8.16667 16.6667 6.66667 15.1778 6.66667 13.3333H2.21111V2.22222H17.7778V13.3333Z"
        fill={color}
      />
    </Svg>
  );
}
