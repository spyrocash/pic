import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function GalleryFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 16;
  const viewBoxHeight = 10;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M.667.333H2v9.334H.667V.333zm2.666 0h1.334v9.334H3.333V.333zm11.334 0h-8C6.3.333 6 .633 6 1v8c0 .367.3.667.667.667h8c.366 0 .666-.3.666-.667V1c0-.367-.3-.667-.666-.667zm-7.334 8L9 6.234l1.193 1.434L11.86 5.52 14 8.334H7.333z"
        fill={color}
      />
    </Svg>
  );
}
