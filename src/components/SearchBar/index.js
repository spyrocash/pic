import React from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from './styles';
import SearchOutlined from '../Icon/SearchOutlined';
import CancelFilled from '../Icon/CancelFilled';

export default function SearchBar() {
  return (
    <View style={styles.container}>
      <SearchOutlined style={styles.searchIcon} />
      <TextInput style={styles.input} placeholder="Search" placeholderTextColor="#8E8E93" />
      <CancelFilled />
    </View>
  );
}
