import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function RightOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 10;
  const viewBoxHeight = 17;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M.843 14.862a1.26 1.26 0 00-.038 1.685c.42.476 1.118.495 1.559.04l6.82-7.133a1.303 1.303 0 00-.036-1.741L2.364.622A1.045 1.045 0 00.805.66a1.26 1.26 0 00.036 1.684l5.564 5.917a.5.5 0 010 .685L.843 14.862z"
        fill={color}
      />
    </Svg>
  );
}
