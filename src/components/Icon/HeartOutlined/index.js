import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function HeartOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 18;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M14.125.75A5.49 5.49 0 0010 2.666 5.49 5.49 0 005.875.75C3.052.75.833 2.968.833 5.792c0 3.465 3.117 6.288 7.838 10.578L10 17.57l1.33-1.21c4.72-4.28 7.837-7.103 7.837-10.568 0-2.824-2.219-5.042-5.042-5.042zm-4.033 14.254l-.092.092-.092-.092c-4.363-3.95-7.241-6.563-7.241-9.212 0-1.834 1.375-3.209 3.208-3.209 1.412 0 2.787.908 3.273 2.164h1.714c.476-1.256 1.851-2.164 3.263-2.164 1.833 0 3.208 1.375 3.208 3.209 0 2.649-2.878 5.261-7.241 9.212z"
        fill={color}
      />
    </Svg>
  );
}
