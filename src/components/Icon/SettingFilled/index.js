import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function SettingFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 16;
  const viewBoxHeight = 16;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M13.355 8.705c.03-.225.045-.458.045-.705 0-.24-.015-.48-.053-.705L14.87 6.11a.368.368 0 00.09-.458l-1.44-2.49a.366.366 0 00-.442-.165l-1.793.72a5.299 5.299 0 00-1.215-.705L9.8 1.107A.363.363 0 009.44.8H6.56a.355.355 0 00-.353.307l-.27 1.905c-.442.18-.847.428-1.215.705l-1.792-.72a.358.358 0 00-.443.165l-1.432 2.49c-.09.158-.06.353.09.458l1.522 1.185A4.402 4.402 0 002.6 8c0 .232.015.48.052.705L1.13 9.89a.368.368 0 00-.09.457l1.44 2.49c.09.165.277.218.442.165l1.793-.72c.375.285.772.525 1.215.705l.27 1.905c.037.18.18.308.36.308h2.88a.35.35 0 00.352-.308l.27-1.905c.443-.18.848-.42 1.216-.705l1.792.72c.165.06.352 0 .442-.165l1.44-2.49c.09-.165.053-.352-.09-.457l-1.507-1.185zM8 10.7A2.708 2.708 0 015.3 8c0-1.485 1.215-2.7 2.7-2.7s2.7 1.215 2.7 2.7-1.215 2.7-2.7 2.7z"
        fill={color}
      />
    </Svg>
  );
}
