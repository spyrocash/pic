import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function SearchOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 14;
  const viewBoxHeight = 15;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.51 11.51a5.5 5.5 0 114.304-2.075.657.657 0 01.156.115l3.818 3.819a.65.65 0 01-.92.92l-3.818-3.82a.653.653 0 01-.115-.155A5.475 5.475 0 015.51 11.51zm4.5-5.5a4.5 4.5 0 11-9 0 4.5 4.5 0 019 0z"
        fill={color}
      />
    </Svg>
  );
}
