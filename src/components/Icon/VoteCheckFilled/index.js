import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function VoteCheckFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 18;
  const viewBoxHeight = 16;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M17.25 8l-1.83-2.093.255-2.767-2.708-.615-1.417-2.4L9 1.22 6.45.125 5.032 2.518l-2.707.607L2.58 5.9.75 8l1.83 2.092-.255 2.775 2.707.615 1.418 2.393L9 14.773l2.55 1.095 1.417-2.393 2.708-.615-.255-2.768L17.25 8zm-9.683 3.54l-2.85-2.857 1.11-1.11 1.74 1.747 4.388-4.402 1.11 1.11-5.497 5.512z"
        fill={color}
      />
    </Svg>
  );
}
