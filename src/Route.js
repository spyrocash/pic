import React, { useContext } from 'react';
import { ThemeContext } from 'react-native-elements';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { Icon } from './components';

import { Home, Trending, Profile, PersonalInfomation, Password, Setting } from './pages';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeNavigator = () => {
  const { theme } = useContext(ThemeContext);
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: theme.colors.primary,
        showLabel: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.HomeOutlined height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Trending"
        component={Trending}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.TrophyOutlined height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Post"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.HomeOutlined width={24} height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Notification"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.BellOutlined width={24} height={24} color={color} />;
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ color }) => {
            return <Icon.UserCircleOutlined width={24} height={24} color={color} />;
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default function Route() {
  const { theme } = useContext(ThemeContext);
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={HomeNavigator} />
      <Stack.Screen name="Setting" component={Setting} />
      <Stack.Screen name="Personal Information" component={PersonalInfomation} />
      <Stack.Screen name="Password" component={Password} />
    </Stack.Navigator>
  );
}
