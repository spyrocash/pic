import HomeOutlined from './HomeOutlined';
import TrophyOutlined from './TrophyOutlined';
import BellOutlined from './BellOutlined';
import UserCircleOutlined from './UserCircleOutlined';
import ArrowLeftIcon from './ArrowLeftIcon';
import InboxOutlined from './InboxOutlined';
import CancelFilled from './CancelFilled';
import TagDialogFiled from './TagDialogFiled';
import LeftOutlined from './LeftOutlined';
import RightOutlined from './RightOutlined';
import HashtagOutlined from './HashtagOutlined';
import SaveOutlined from './SaveOutlined';
import MoreOutlined from './MoreOutlined';
import HeartOutlined from './HeartOutlined';
import BarChartOutlined from './BarChartOutlined';
import LikeOutlined from './LikeOutlined';
import CloseOutlined from './CloseOutlined';
import NotificationOutlined from './NotificationOutlined';
import CheckOutlined from './CheckOutlined';
import UserOutlined from './UserOutlined';
import CloseCircleFilled from './CloseCircleFilled';
import CommentFilled from './CommentFilled';
import GalleryFilled from './GalleryFilled';
import HeartFilled from './HeartFilled';
import UserFilled from './UserFilled';
import UnlockFilled from './UnlockFilled';
import LanguageFilled from './LanguageFilled';
import QuestionCircleFilled from './QuestionCircleFilled';
import TncFilled from './TncFilled';
import SettingFilled from './SettingFilled';
import VoteCheckFilled from './VoteCheckFilled';
import CrownIcon from './CrownIcon';
import SilverStarIcon from './SilverStarIcon';

export default {
  HomeOutlined,
  TrophyOutlined,
  BellOutlined,
  UserCircleOutlined,
  ArrowLeftIcon,
  InboxOutlined,
  CancelFilled,
  TagDialogFiled,
  LeftOutlined,
  RightOutlined,
  InboxOutlined,
  HashtagOutlined,
  SaveOutlined,
  MoreOutlined,
  HeartOutlined,
  BarChartOutlined,
  LikeOutlined,
  CloseOutlined,
  NotificationOutlined,
  CheckOutlined,
  UserOutlined,
  CloseCircleFilled,
  CommentFilled,
  GalleryFilled,
  HeartFilled,
  UserFilled,
  UnlockFilled,
  LanguageFilled,
  QuestionCircleFilled,
  TncFilled,
  SettingFilled,
  VoteCheckFilled,
  CrownIcon,
  SilverStarIcon,
};
