import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function QuestionCircleFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zm0 15.833a.833.833 0 110-1.667.833.833 0 010 1.667zm1.32-5.298a.837.837 0 00-.487.758v.374a.833.833 0 11-1.666 0v-.374A2.51 2.51 0 0110.62 9.02c.849-.39 1.462-1.429 1.462-1.938A2.086 2.086 0 0010 5c-1.15 0-2.083.935-2.083 2.083a.833.833 0 11-1.667 0A3.755 3.755 0 0110 3.333a3.755 3.755 0 013.75 3.75c0 1.126-.977 2.781-2.43 3.452z"
        fill={color}
      />
    </Svg>
  );
}
