import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';
import ArrowLeft from '../Icon/ArrowLeftIcon';
import InboxOutlined from '../Icon/InboxOutlined';

export default function HeaderHome({ onPressInbox }) {
  function onGoBack() {}

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => onGoBack()}>
        <ArrowLeft style={styles.arrowLeft} />
      </TouchableOpacity>
      <Text>Logo</Text>
      <TouchableOpacity onPress={onPressInbox}>
        <InboxOutlined style={styles.Inbox} />
      </TouchableOpacity>
    </View>
  );
}
