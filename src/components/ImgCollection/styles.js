import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {},

  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  label: {
    marginRight: 8,
    fontSize: 16,
    fontWeight: '600',
  },
  imgWrap: {},
  imgContainer: {
    position: 'relative',
    marginHorizontal: 0.5,
  },
  imgLabel: {
    paddingTop: 8,
    fontSize: 12,
    fontWeight: '400',
  },
  rankNo: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 6,
    right: 6,
    width: 18,
    height: 18,
    backgroundColor: '#FFFFFF',
    borderRadius: 18,
  },
  textRankNo: {
    fontSize: 12,
    fontWeight: '600',
  },
});

export default styles;
