export { default as Home } from './Home';
export { default as Trending } from './Trending';
export { default as Profile } from './Profile';
export { default as Setting } from './Setting';
export { default as PersonalInfomation } from './PersonalInfomation';
export { default as Password } from './Password';
export { default as Explore } from './Explore';
export { default as Following } from './Following';
