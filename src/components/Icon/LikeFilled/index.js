import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function LikeFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 20;
  const viewBoxHeight = 20;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M6.25 8.75H2.083A2.086 2.086 0 000 10.833V17.5c0 1.148.935 2.083 2.083 2.083h2.5A2.086 2.086 0 006.667 17.5V9.167a.417.417 0 00-.417-.417zM20 11.875c0-.5-.2-.967-.542-1.313.39-.425.588-.997.533-1.59-.098-1.06-1.053-1.889-2.174-1.889h-5.146c.254-.774.662-2.193.662-3.333 0-1.807-1.536-3.333-2.5-3.333-.866 0-1.484.486-1.51.507-.1.08-.157.2-.157.326v2.825L7.293 8.133a.417.417 0 00.022.39c.122.203.185.42.185.643v8.333c0 .201-.024.405-.072.626a.416.416 0 00.315.496 4.8 4.8 0 001.007.128h7.649c.907 0 1.702-.611 1.887-1.456.096-.434.04-.872-.15-1.251a1.867 1.867 0 00.835-2.501A1.865 1.865 0 0020 11.875z"
        fill={color}
      />
    </Svg>
  );
}
