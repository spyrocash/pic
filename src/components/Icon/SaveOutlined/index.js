import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function SaveOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 12;
  const viewBoxHeight = 17;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M11.423 0H.577C.258 0 0 .272 0 .608v15.784c0 .236.13.45.332.55.203.1.443.068.615-.083L6 12.417l5.054 4.442a.558.558 0 00.614.084.61.61 0 00.332-.55V.607C12 .272 11.742 0 11.423 0zm-.576 15.094L6.37 11.159a.559.559 0 00-.74 0l-4.476 3.935V1.216h9.693v13.878z"
        fill={color}
      />
    </Svg>
  );
}
