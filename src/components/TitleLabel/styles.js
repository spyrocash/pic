import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: '600',
  },
});

export default styles;
