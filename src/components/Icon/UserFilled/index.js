import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function UserFilled({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 13;
  const viewBoxHeight = 16;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.212 7.815h.12C3.774 7.323 2.636 5.785 2.636 4c0-2.215 1.737-4 3.894-4s3.894 1.785 3.894 4c0 1.785-1.138 3.323-2.756 3.815h.12c2.876 0 5.212 2.4 5.212 5.354V16H0v-2.83c0-2.955 2.337-5.355 5.212-5.355z"
        fill={color}
      />
    </Svg>
  );
}
