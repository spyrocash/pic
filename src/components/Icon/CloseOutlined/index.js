import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function CloseOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 18;
  const viewBoxHeight = 18;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M10.71 9l6.49 6.49a1.22 1.22 0 010 1.71 1.22 1.22 0 01-1.71 0L9 10.71 2.51 17.2a1.22 1.22 0 01-1.71 0 1.22 1.22 0 010-1.71L7.29 9 .8 2.51A1.22 1.22 0 01.8.8a1.22 1.22 0 011.71 0L9 7.29 15.49.8a1.22 1.22 0 011.71 0 1.22 1.22 0 010 1.71L10.71 9z"
        fill={color}
      />
    </Svg>
  );
}
