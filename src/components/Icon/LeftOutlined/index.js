import React from 'react';
import Svg, { Path } from 'react-native-svg';

import { aspectRatioCalculate } from '../../../utils';

export default function LeftOutlined({ width, height, color = '#4E5B7E' }) {
  const viewBoxWidth = 12;
  const viewBoxHeight = 23;

  const { newWidth, newHeight } = aspectRatioCalculate({
    width,
    height,
    originalWidth: viewBoxWidth,
    originalHeight: viewBoxHeight,
  });

  return (
    <Svg
      width={newWidth}
      height={newHeight}
      viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
      fill="none">
      <Path
        d="M11.542 3.183c.588-.605.61-1.61.05-2.246A1.393 1.393 0 009.516.883S.445 10.367.42 10.395c-.58.655-.558 1.694.048 2.321l9.046 9.455a1.393 1.393 0 002.078-.05c.56-.635.54-1.64-.047-2.247L4.02 11.871a.5.5 0 010-.685l7.522-8.003z"
        fill={color}
      />
    </Svg>
  );
}
