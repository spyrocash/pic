import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:'center',
    height: 55,
    paddingLeft: 26,
    paddingRight: 22,
  },
  arrowLeft: {},
  Inbox: {},
  logoText: {
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export default styles;
