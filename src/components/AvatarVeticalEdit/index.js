import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import { Avatar } from 'react-native-elements';

export default function AvatarVeticalEdit(props) {
  const { imgPath, label1 } = props;
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.avatarWrap}>
        <Avatar rounded source={imgPath} size={40} />
      </View>
      <Text style={styles.label1}>{label1}</Text>
    </TouchableOpacity>
  );
}
