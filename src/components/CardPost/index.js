import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import CommentFilled from '../Icon/CommentFilled';
import ProfileCover from '../ProfileCover';
import MoreOutlined from '../Icon/MoreOutlined';
import SaveOutlined from '../Icon/SaveOutlined';
import HeartOutlined from '../Icon/HeartOutlined';
import BarChartOutlined from '../Icon/BarChartOutlined';
import HrLine from '../HrLine';

export default function CardPost({ votingMode = false }) {
  return (
    <View style={{ paddingHorizontal: 16, marginBottom: 20 }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingBottom: 8,
        }}>
        <ProfileCover />
        <Text style={{ marginLeft: 16, fontWeight: '600', fontSize: 16, flex: 1 }}>Username</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity style={{ paddingHorizontal: 10 }}>
            <SaveOutlined />
          </TouchableOpacity>
          <View style={{ marginRight: 10 }} />
          <TouchableOpacity style={{ paddingHorizontal: 10 }}>
            <MoreOutlined />
          </TouchableOpacity>
        </View>
      </View>

      <View>
        <Image
          source={require('../../../assets/images/watch.png')}
          style={{ width: '100%', height: 343 }}
          resizeMode="cover"
        />
        {/* Time */}
        <View
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            margin: 11,
            paddingHorizontal: 16,
            borderRadius: 20,
            paddingVertical: 4,
          }}>
          <Text style={{ color: '#fff', fontWeight: '600' }}>0:15</Text>
        </View>
        {/* Swipe item */}
        <View
          style={{
            position: 'absolute',
            backgroundColor: '#E2E4EF',
            bottom: 0,
            right: 0,
            margin: 11,
            paddingHorizontal: 16,
            borderRadius: 20,
            paddingVertical: 4,
          }}>
          <Text>1/5</Text>
        </View>
      </View>

      <View style={{ paddingVertical: 10, flexDirection: 'row' }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <HeartOutlined />
          <Text>0 like</Text>
          <Text>0 vote</Text>
          <Text>23 hours 59 minutes left</Text>
        </View>

        <View style={{ flexDirection: 'row' }}>
          <Text style={{ marginRight: 10 }}>(0)</Text>
          <CommentFilled />
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'baseline',
        }}>
        <Text style={{ fontWeight: '600', fontSize: 16 }}>Username</Text>
        <Text style={{ fontSize: 14, fontWeight: '400', marginLeft: 8 }}>
          Lorum Ipsum #watch#brandname
        </Text>
      </View>
      <HrLine />
      {votingMode ? (
        <View>
          <Text>Voting Mode</Text>
        </View>
      ) : (
        <View>
          <TouchableOpacity style={{ flexDirection: 'row' }}>
            <BarChartOutlined />
            <Text style={{ marginLeft: 10, color: '#696CE0', fontSize: 14, fontWeight: '400' }}>
              View post activity
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
}
