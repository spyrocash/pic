import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import { Icon } from 'react-native-elements';

export default function MenuItem(props) {
  const { iconLName, iconLType, iconRName, iconRType, text, handlePress } = props;
  return (
    <TouchableOpacity style={styles.container} onPress={handlePress}>
      <View style={styles.leftCol}>
        <Icon name={iconLName} type={iconLType} color="#4E5B7E" />
      </View>
      <View style={styles.rightCol}>
        <Text style={styles.text}>{text}</Text>
        <Icon name={iconRName} type={iconRType} color="#4E5B7E" />
      </View>
    </TouchableOpacity>
  );
}
