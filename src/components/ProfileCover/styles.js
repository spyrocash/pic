import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  image: { width: 40, height: 40, borderRadius: 40 / 2 },
  iconRank: { position: 'absolute', right: -4, top: -6 },
});

export default styles;
