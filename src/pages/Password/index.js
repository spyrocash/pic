import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { InputTextHorizontal } from '../../components';

export default function Password() {
  return (
    <View style={styles.passwordContainerLayout}>
      <InputTextHorizontal placeholder="Current password" placeholderTextColor="#B1B9D1" />
      <InputTextHorizontal placeholder="New password" placeholderTextColor="#B1B9D1" />
      <InputTextHorizontal placeholder="Re-enter new password" placeholderTextColor="#B1B9D1" />
    </View>
  );
}
